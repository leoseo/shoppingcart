package com.cart.common.constant;

public interface Constant {
	
	public final static String SUCCESS = "SUCCESS";
	public final static String ERROR = "ERROR";
	
	public final static String AUTHORIZATION_FIELD = "Authorization";
	public final static String TOKEN_TYPE = "Bearer ";
	
	public final static String KEY = "expiredtoken";

}
