package com.cart.common.exception;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cart.common.model.ResponseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class CartExceptionHandler {

	@Autowired
	@Qualifier("customMapper")
	private ObjectMapper mapper;

	@ExceptionHandler(CartException.class)
	public @ResponseBody ResponseMessage handle(CartException e, HttpServletResponse response) {
		log.error("Custom error received: {}", e);
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return new ResponseMessage(HttpStatus.BAD_REQUEST.value(), e.getClass().getSimpleName(), e.getMessage());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public @ResponseBody ResponseMessage handle(MethodArgumentNotValidException e, HttpServletResponse response) {
		log.error("Validation error received: {}", e);
		List<String> errors = e.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		return new ResponseMessage(HttpStatus.BAD_REQUEST.value(), e.getClass().getSimpleName(), errors);
	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody ResponseMessage handle(Exception e, HttpServletResponse response) {
		log.error("Unknown error received: {}", e);
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getClass().getSimpleName(), e.getMessage());
	}

}
