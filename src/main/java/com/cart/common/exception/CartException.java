package com.cart.common.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

public class CartException extends RuntimeException {

	private static final long serialVersionUID = 7374076822728601201L;
	
	@Getter
	private int code;
	
	public CartException(String msg) {
		super(msg);
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
	}

	public CartException(int code, String msg) {
		super(msg);
		this.code = code;
	}
	
}
