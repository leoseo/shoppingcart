package com.cart.common.util;

import java.text.MessageFormat;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CartUtils {
	
	public static String toJsonString(ObjectMapper mapper, Object o) {
		try {
			return mapper.writeValueAsString(o);
		} catch (Exception e) {
			log.warn("Error printing: {}", o);
		}
		return "";
	}
	
	public static String format(String template, Object... parts) {
		try {
			return MessageFormat.format(template, parts);
		} catch (Exception e) {
			return "Error converting template: " + template;
		}
	}

}
