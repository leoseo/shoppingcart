package com.cart.common.validator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.cart.common.entity.SRole;


@Component
public class PermissionValidator implements ConstraintValidator<StaffPermission, Set<String>> {

	private List<String> permissions = null;

	@Override
	public boolean isValid(Set<String> values, ConstraintValidatorContext context) {

		if (permissions == null) {// caching
			permissions = Stream.of(SRole.values()).map(e -> e.name()).collect(Collectors.toList());
		}
		
		if (!CollectionUtils.isEmpty(values)) {
			for (String value : values) {
				if (!permissions.contains(value)) {
					return false;
				}
			}
		}
		return true;
	}

}
