package com.cart.common.validator;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = PermissionValidator.class)
@Documented
public @interface StaffPermission {
	
	String message() default "Permission is unavailable";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
