package com.cart.common.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cart.common.entity.Staff;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Integer> {
	
	List<Staff> findByActive(Boolean active);
	
	Optional<Staff> findByUsername(String username);
	
	Boolean existsByUsername(String username);
	
	Boolean existsByEmail(String email);

}
