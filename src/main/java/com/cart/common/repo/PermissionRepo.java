package com.cart.common.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cart.common.entity.Permission;

@Repository
public interface PermissionRepo extends JpaRepository<Permission, Integer>  {
	
	Optional<Permission> findByRole(String role);
	
}
