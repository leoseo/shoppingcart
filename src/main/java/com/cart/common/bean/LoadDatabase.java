package com.cart.common.bean;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cart.common.entity.Permission;
import com.cart.common.entity.Product;
import com.cart.common.entity.SRole;
import com.cart.common.entity.Staff;
import com.cart.common.repo.PermissionRepo;
import com.cart.common.repo.ProductRepository;
import com.cart.common.repo.StaffRepository;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class LoadDatabase {
	
	@Autowired
	PasswordEncoder encoder;

	@Bean
	CommandLineRunner initDatabase(ProductRepository productRepo, StaffRepository staffRepo,
			PermissionRepo permissionRepo) {
		
		String ext = ".jpeg";

		return args -> {
			Product p1 = Product.builder().id(1).productName("Dumpling").availableQuantity(20).price(new BigDecimal("30.99"))
					.active(true).imagePath("/dumpling" + ext).build();
			Product p2 = Product.builder().id(2).productName("Fish Sauce").availableQuantity(35).price(new BigDecimal("5.99"))
					.active(true).imagePath("/fish_sauce" + ext).build();
			Product p3 = Product.builder().id(3).productName("Rice").availableQuantity(10).price(new BigDecimal("10.99"))
					.active(true).imagePath("/rice" + ext).build();
			Product p4 = Product.builder().id(4).productName("Soy Sauce").availableQuantity(35).price(new BigDecimal("6.99"))
					.active(true).imagePath("/soy_sauce" + ext).build();
			Product p5 = Product.builder().id(5).productName("Steamed Buns").availableQuantity(40).price(new BigDecimal("8.99"))
					.active(true).imagePath("/steamed_buns" + ext).build();
			log.info("Preloading " + productRepo.saveAll(new ArrayList<Product>() {{add(p1); add(p2);add(p3); add(p4);add(p5);}}));

			Permission pm1 = Permission.builder().id(1).role(SRole.ROLE_ADMINISTRATOR.name()).description("Administrator").build();
			Permission pm2 = Permission.builder().id(2).role(SRole.ROLE_STAFF.name()).description("Sale Staff").build();
			log.info("Preloading " + permissionRepo.saveAll(new ArrayList<Permission>() {{add(pm1); add(pm2);}}));
			
			String tmpPw = encoder.encode("123456");
			Staff s1 = Staff.builder().id(1).username("admin").password(tmpPw).fullName("Administrator 1").active(true)
					.email("123@gmail.com").phone("123456").permission(pm1).build();
			Staff s2 = Staff.builder().id(2).username("sale").password(tmpPw).fullName("Sale Staff 1").active(true)
					.email("456@gmail.com").phone("123456").permission(pm2).build();
			Staff s3 = Staff.builder().id(3).username("superuser").password(tmpPw).fullName("CEO 1").active(true)
					.email("789@gmail.com").phone("123456").permission(pm1).permission(pm2).build();
			log.info("Preloading " + staffRepo.saveAll(new ArrayList<Staff>() {{add(s1); add(s2); add(s3);}}));
		};
	}

}
