package com.cart.common.event;

import java.io.Serializable;

import org.springframework.context.ApplicationEvent;

import lombok.Getter;
import lombok.Setter;

public class ListenerEvent extends ApplicationEvent implements Serializable {

	private static final long serialVersionUID = 6263796035123172778L;

	@Getter
	@Setter
	private String info;

	public ListenerEvent(Object source, String info) {
		super(source);
		this.info = info;
	}

}
