package com.cart.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages={"com.cart.common"})
@EnableTransactionManagement
@EntityScan(basePackages="com.cart.common.entity")
@EnableJpaRepositories(basePackages="com.cart.common.repo")
public class AssignmentHdApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssignmentHdApplication.class, args);
	}

}
