package com.cart.common.security.jwt;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cart.common.constant.Constant;
import com.cart.common.security.service.StaffDetailsImpl;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JWTUtils {

	@Value("${cart.app.jwtSecret}")
	private String jwtSecret;

	@Value("${cart.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	public String generateJwtToken(Authentication authentication) {

		StaffDetailsImpl staffPrincipal = (StaffDetailsImpl) authentication.getPrincipal();
		Date start = new Date();
		Date end = new Date(start.getTime() + jwtExpirationMs);

		return Jwts.builder().setSubject(staffPrincipal.getUsername()).setIssuedAt(new Date()).setExpiration(end)
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
	}

	public String getUserNameFromJwtToken(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			log.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			log.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			log.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			log.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			log.error("JWT claims string is empty: {}", e.getMessage());
		}
		return false;
	}
	
	public String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader(Constant.AUTHORIZATION_FIELD);

		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith(Constant.TOKEN_TYPE)) {
			return headerAuth.substring(Constant.TOKEN_TYPE.length(), headerAuth.length());
		}

		return null;
	}
}
