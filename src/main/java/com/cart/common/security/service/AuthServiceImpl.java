package com.cart.common.security.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cart.common.constant.Constant;
import com.cart.common.entity.Permission;
import com.cart.common.entity.Staff;
import com.cart.common.event.ListenerEvent;
import com.cart.common.exception.CartException;
import com.cart.common.model.BlacklistDTO;
import com.cart.common.model.StaffDTO;
import com.cart.common.repo.PermissionRepo;
import com.cart.common.repo.StaffRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AuthServiceImpl implements AuthService {

	@Autowired
	StaffRepository staffRepo;

	@Autowired
	PermissionRepo permissionRepo;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Override
	public void registerUser(StaffDTO dto) {

		if (staffRepo.existsByUsername(dto.getUsername())) {
			throw new CartException(HttpStatus.BAD_REQUEST.value(), "Username is already taken!");
		}

		if (staffRepo.existsByEmail(dto.getEmail())) {
			throw new CartException(HttpStatus.BAD_REQUEST.value(), "Email is already taken!");
		}

		Set<String> tmp = dto.getPermissions();
		Set<Permission> roles = new HashSet<Permission>();
		for (String p : tmp) {
			Permission role = permissionRepo.findByRole(p)
					.orElseThrow(() -> new CartException(HttpStatus.NOT_FOUND.value(), "Role not found " + p));
			roles.add(role);
		}
		Staff staff = Staff.builder().username(dto.getUsername()).password(encoder.encode(dto.getPassword()))
				.fullName(dto.getFullName()).email(dto.getEmail()).phone(dto.getPhone()).permissions(roles).build();
		staffRepo.save(staff);
	}

	@Override
	public boolean isInvalid(String username, String authJwt) {
		try {
			String expiredJwt = (String) redisTemplate.opsForHash().get(Constant.KEY, username);
			if (expiredJwt != null) {
				log.info("Found blacklisted user {} - token {}", username, expiredJwt);
				return authJwt.equals(expiredJwt);
			}
		} catch (Exception e) {
			log.error("Error while finding expired token: {}", e);
		}
		return false;
	}

	@Override
	public void saveBlacklistToken(BlacklistDTO dto) {
		ListenerEvent event = new ListenerEvent(dto, "EXPIRED_TOKEN");
		publisher.publishEvent(event); // publish to save later, avoid saving exception
	}
	
	/**
	 * This can be moved to any @Service @Transactional class
	 * @param event listening event
	 */
	@EventListener({ListenerEvent.class})
	public void saveToken(ListenerEvent event) {
		BlacklistDTO dto = (BlacklistDTO) event.getSource();
		redisTemplate.opsForHash().put(Constant.KEY, dto.getUsername(), dto.getJwt());
	}
}
