package com.cart.common.security.service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.cart.common.entity.Staff;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;

public class StaffDetailsImpl implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Getter
	private Integer id;

	private String username;

	@Getter
	private String email;

	@JsonIgnore
	private String password;

	private Collection<? extends GrantedAuthority> authorities;

	public StaffDetailsImpl(Integer id, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}

	public static StaffDetailsImpl build(Staff staff) {
		List<GrantedAuthority> authorities = staff.getPermissions().stream()
				.map(p -> new SimpleGrantedAuthority(p.getRole())).collect(Collectors.toList());
		return new StaffDetailsImpl(staff.getId(), staff.getUsername(), staff.getEmail(), staff.getPassword(),
				authorities);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		StaffDetailsImpl user = (StaffDetailsImpl) o;
		return Objects.equals(id, user.id);
	}

}
