package com.cart.common.security.service;

import com.cart.common.model.BlacklistDTO;
import com.cart.common.model.StaffDTO;

public interface AuthService {
	
	public void registerUser(StaffDTO dto);
	
	public boolean isInvalid(String username, String jwt);
	
	public void saveBlacklistToken(BlacklistDTO dto);
	
}
