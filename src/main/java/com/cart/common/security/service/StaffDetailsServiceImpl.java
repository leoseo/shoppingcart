package com.cart.common.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cart.common.entity.Staff;
import com.cart.common.repo.StaffRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class StaffDetailsServiceImpl implements UserDetailsService {

	@Autowired
	StaffRepository staffRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.info("Finding staff details for {}", username);
		Staff staff = staffRepo.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("Staff username not found: " + username));
		return StaffDetailsImpl.build(staff);
	}

}
