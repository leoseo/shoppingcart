package com.cart.common.service;

import java.util.List;

import com.cart.common.model.StaffDTO;

public interface StaffService {
	
	public List<StaffDTO> list();
	
	public boolean update(StaffDTO staff);
	
	public boolean remove(int id);

}
