package com.cart.common.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cart.common.entity.Staff;
import com.cart.common.exception.CartException;
import com.cart.common.model.StaffDTO;
import com.cart.common.repo.PermissionRepo;
import com.cart.common.repo.StaffRepository;
import com.cart.common.service.StaffService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class StaffServiceImpl implements StaffService {

	@Autowired
	StaffRepository staffRepo;

	@Autowired
	PermissionRepo permissionRepo;

	@Override
	public List<StaffDTO> list() {

		log.info("Finding all staffs ....");
		List<Staff> staffs = staffRepo.findByActive(true);
		if (CollectionUtils.isEmpty(staffs)) {
			String msg = "No staff available!";
			log.info(msg + " Returning ..");
			throw new CartException(HttpStatus.NOT_FOUND.value(), msg);
		}

		List<StaffDTO> results = staffs.stream().map(r -> {
			List<String> permissions = r.getPermissions().stream().map(p -> p.getRole()).collect(Collectors.toList());
			StaffDTO dto = StaffDTO.builder().id(r.getId()).username(r.getUsername()).fullName(r.getFullName())
					.email(r.getEmail()).phone(r.getPhone()).permissions(permissions).build();
			return dto;
		}).collect(Collectors.toList());
		return results;
	}

	@Override
	public boolean update(StaffDTO staff) {
		// to do next
		return false;
	}

	@Override
	public boolean remove(int id) {
		// to do next
		return false;
	}

}
