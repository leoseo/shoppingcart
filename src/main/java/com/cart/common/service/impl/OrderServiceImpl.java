package com.cart.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cart.common.entity.Product;
import com.cart.common.exception.CartException;
import com.cart.common.model.OrderDTO;
import com.cart.common.repo.OrderRepo;
import com.cart.common.repo.ProductRepository;
import com.cart.common.service.OrderService;
import com.cart.common.util.CartUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepo orderRepo;

	@Autowired
	ProductRepository productRepo;

	@Override
	public boolean addOrder(OrderDTO dto) {

		Product product = productRepo.findByProductName(dto.getProductName())
				.orElseThrow(() -> new CartException(HttpStatus.NOT_FOUND.value(),
						CartUtils.format("Product {0} is not available", dto.getProductName())));
		
//		if (product.getAvailableQuantity() < dto.get) {
//			
//		}
		

		return false;
	}

}
