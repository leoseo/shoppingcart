package com.cart.common.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cart.common.entity.Product;
import com.cart.common.exception.CartException;
import com.cart.common.model.ProductDTO;
import com.cart.common.repo.ProductRepository;
import com.cart.common.service.ProductService;
import com.cart.common.util.CartUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepo;

	@Override
	public List<ProductDTO> getAllProducts() {

		log.info("Finding all products ....");
		List<Product> products = productRepo.findByActive(true);
		if (CollectionUtils.isEmpty(products)) {
			String msg = "No products available!";
			log.info(msg + " Returning ..");
			throw new CartException(HttpStatus.NOT_FOUND.value(), msg);
		}

		List<ProductDTO> results = products.stream()
				.map(r -> ProductDTO.builder().id(r.getId()).productName(r.getProductName())
						.availableQuantity(r.getAvailableQuantity()).price(r.getPrice())
						.imagePath(r.getImagePath()).build())
				.collect(Collectors.toList());
		log.info("Converted entity list to dto list");
		return results;
	}

	@Override
	public boolean add(ProductDTO dto) {
		log.info("Adding a product ....{}", dto);
		
		if (productRepo.findById(dto.getId()).isPresent()) {
			String msg = "Product already existed!";
			log.error(msg);
			throw new CartException(HttpStatus.BAD_REQUEST.value(), msg);
		}
		
		Product product = new Product();
		BeanUtils.copyProperties(dto, product);
		product.setActive(true);
		productRepo.save(product);
		return true;
	}

	@Override
	public boolean update(ProductDTO dto) {
		log.info("Updating a product ....{}", dto);
		Product result = findProduct(dto.getId());
		productRepo.save(result);
		return true;
	}

	@Override
	public boolean remove(int productId) {
		log.info("Disabling a product ....{}", productId);
		Product result = findProduct(productId);
		result.setActive(false);
		productRepo.save(result);
		return true;
	}

	private Product findProduct(int id) {
		return productRepo.findById(id).orElseThrow(() -> new CartException(HttpStatus.NOT_FOUND.value(),
				CartUtils.format("Product ID {0} not available", id)));
	}

}
