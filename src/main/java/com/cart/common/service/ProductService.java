package com.cart.common.service;

import java.util.List;

import com.cart.common.model.ProductDTO;

public interface ProductService {

	public List<ProductDTO> getAllProducts();

	public boolean add(ProductDTO dto);

	public boolean update(ProductDTO dto);

	public boolean remove(int productId);
}
