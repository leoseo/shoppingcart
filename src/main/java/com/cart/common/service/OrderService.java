package com.cart.common.service;

import com.cart.common.model.OrderDTO;

public interface OrderService {
	
	public boolean addOrder(OrderDTO order);

}
