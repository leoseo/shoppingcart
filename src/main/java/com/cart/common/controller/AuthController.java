package com.cart.common.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cart.common.model.BlacklistDTO;
import com.cart.common.model.ResponseMessage;
import com.cart.common.model.StaffDTO;
import com.cart.common.model.payload.JWTResponse;
import com.cart.common.model.payload.LoginRequest;
import com.cart.common.security.jwt.JWTUtils;
import com.cart.common.security.service.AuthService;
import com.cart.common.security.service.StaffDetailsImpl;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	AuthService authService;

	@Autowired
	JWTUtils jwtUtils;

	@GetMapping("/test")
	public String test() {
		return "OK";
	}
	
	@PostMapping("/signup")
	public ResponseMessage signup(@Valid @RequestBody StaffDTO dto) {
		authService.registerUser(dto);
		String msg = "Successfully created staff " + dto.getUsername();
		log.info(msg);
		return new ResponseMessage(msg);
	}

	@PostMapping("/signin")
	public ResponseMessage signin(@Valid @RequestBody LoginRequest loginRequest) {

		String username = loginRequest.getUsername();
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(username, loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtUtils.generateJwtToken(authentication);
		StaffDetailsImpl details = (StaffDetailsImpl) authentication.getPrincipal();

		// GrantedAuthority
		List<String> roles = details.getAuthorities().stream().map(x -> x.getAuthority()).collect(Collectors.toList());
		JWTResponse jwtRes = JWTResponse.builder().token(jwt).id(details.getId()).username(details.getUsername())
				.email(details.getEmail()).roles(roles).build();
		log.info("Successfully signed in for user {}", username);
		return new ResponseMessage(jwtRes);
	}

	@PostMapping("/signout")
	@PreAuthorize("hasRole('STAFF') or hasRole('ADMINISTRATOR')")
	public ResponseMessage signout(HttpServletRequest request) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		String username = null;
		try {
			username = ((StaffDetailsImpl) securityContext.getAuthentication().getPrincipal()).getUsername();
			String jwt = jwtUtils.parseJwt(request);
			log.info("Getting user - {}, jwt - {}", username, jwt);
			// an extra feature to invalidate jwt token when logging out
			authService.saveBlacklistToken(BlacklistDTO.builder().username(username).jwt(jwt).build());
		} catch (Exception e) {
			log.warn("Failed to save logout token ...", e);
		}
		securityContext.setAuthentication(null);
		return new ResponseMessage("Successfully signed out for user " + username);
	}

}
