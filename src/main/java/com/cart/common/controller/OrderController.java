package com.cart.common.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cart.common.model.OrderDTO;
import com.cart.common.model.ResponseMessage;
import com.cart.common.service.OrderService;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@PostMapping("/add")
	public ResponseMessage order(@Valid @RequestBody OrderDTO dto) {
		String msg = "Successfully created the order for " + dto.getCustomerName();
		log.info(msg);
		return new ResponseMessage(msg);
	}

}
