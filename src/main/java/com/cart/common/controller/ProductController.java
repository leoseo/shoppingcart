package com.cart.common.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cart.common.model.ProductDTO;
import com.cart.common.model.ResponseMessage;
import com.cart.common.service.ProductService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductService productService;

	@GetMapping("/list")
	public ResponseMessage all() {
		return new ResponseMessage(productService.getAllProducts());
	}

	@PostMapping("/add")
	@PreAuthorize("hasRole('STAFF') or hasRole('ADMINISTRATOR')")
	public ResponseMessage add(@Valid @RequestBody ProductDTO product) {
		return new ResponseMessage(productService.add(product));
	}

	@PostMapping("/update")
	@PreAuthorize("hasRole('STAFF') or hasRole('ADMINISTRATOR')")
	public ResponseMessage update(@Valid @RequestBody ProductDTO product) {
		return new ResponseMessage(productService.update(product));
	}

	@PostMapping("/remove")
	@PreAuthorize("hasRole('STAFF') or hasRole('ADMINISTRATOR')")
	public ResponseMessage remove(
			@Min(value = 1, message = "Product ID must be larger than 0")
			@NotNull(message = "Product ID is required") Integer id) {
		return new ResponseMessage(productService.remove(id));
	}

}
