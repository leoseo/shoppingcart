package com.cart.common.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cart.common.model.ResponseMessage;
import com.cart.common.model.StaffDTO;
import com.cart.common.security.service.AuthService;
import com.cart.common.service.StaffService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/staff")
@PreAuthorize("hasRole('ADMINISTRATOR')")
@Slf4j
public class StaffController {

	@Autowired
	StaffService staffService;
	
	@Autowired
	AuthService authService;

	@GetMapping("/list")
	public ResponseMessage list() {
		return new ResponseMessage(staffService.list());
	}

	@PostMapping("/add")
	public ResponseMessage add(@Valid @RequestBody StaffDTO staff) {
		authService.registerUser(staff);
		String msg = "Successfully created staff " + staff.getUsername();
		log.info(msg);
		return new ResponseMessage(msg);
	}

	@PostMapping("/update")
	public ResponseMessage update(@Valid @RequestBody StaffDTO staff) {
		return new ResponseMessage(staffService.update(staff));
	}

	@PostMapping("/remove")
	public ResponseMessage remove(
			@Min(value = 1, message = "Staff ID must be larger than 0")
			@NotNull(message = "Staff ID is required") Integer id) {
		return new ResponseMessage(staffService.remove(id));
	}

}
