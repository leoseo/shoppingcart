package com.cart.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Permission")
public class Permission {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

//	@Column(name = "Role")
//	private String role;

	@Column(name = "Description")
	private String description;

//	// changed for preloading database,
//	// Regarding real db, unless there is a need for eager loading, it should be
//	// left as default (LAZY)
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "permission", cascade = CascadeType.ALL)
//	private List<Staff> staffs;
	
	@Column(length = 20)
	private String role;

}
