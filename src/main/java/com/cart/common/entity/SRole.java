package com.cart.common.entity;

public enum SRole {
	
	/* Spring Security automatically prefixes any role with ROLE_
	 * e.g we defined roles like (ROLE_STAFF)
	 * Spring security automatically added ROLE_ when checking in controller
	 * @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	 */
	ROLE_STAFF,
    ROLE_ADMINISTRATOR;
}
