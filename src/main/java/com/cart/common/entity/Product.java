package com.cart.common.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Product", 
		uniqueConstraints = {
			@UniqueConstraint(columnNames = "productName")
		})
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "ProductName")
	private String productName;

	@Column(name = "AvailableQuantity")
	private Integer availableQuantity;

	@Column(name = "Price")
	private BigDecimal price;

	@Column(name = "Active")
	private Boolean active;
	
	@Column(name="ImagePath")
	private String imagePath;
	
	// changed for preloading database,
	// Regarding real db, unless there is a need for eager loading, it should be
	// left as default (LAZY)
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private List<Order> orders;
}