package com.cart.common.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CartOrder") // Order is a reversed keyword for H2 DB
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@ManyToOne()
	@JoinColumn(name = "ProductID", referencedColumnName = "id")
	private Product product;
	
	@Column(name = "CustomerName")
	private String customerName; // can be upgraded to customerId joining with another table
	
	@Column(name = "Price")
	private BigDecimal price;

}
