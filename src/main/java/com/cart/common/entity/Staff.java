package com.cart.common.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(	name = "Staff",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "userName"),
				@UniqueConstraint(columnNames = "email")
		})
public class Staff {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;

	@Column(name = "username")
	private String username;
	
	@Column(name = "Password")
	private String password;

//	@ManyToOne()
//	@JoinColumn(name = "PermissionID", referencedColumnName = "id")
//	private Permission permission;

	@Column(name = "FullName")
	private String fullName;

	@Column(name = "Email")
	private String email;

	@Column(name = "Phone")
	private String phone;

	@Column(name = "Active")
	private Boolean active;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "StaffPermissions", 
				joinColumns = @JoinColumn(name = "StaffID"), 
				inverseJoinColumns = @JoinColumn(name = "PermissionID"))
	@Singular
	private Set<Permission> permissions = new HashSet<>();
}
