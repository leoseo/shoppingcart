package com.cart.common.model;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.cart.common.validator.StaffPermission;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StaffDTO {

	private Integer id;

	@NotEmpty(message = "Staff User Name is required")
	@Pattern(regexp = "[A-Za-z0-9]+", message = "Staff user name can only contain letters and numbers")
	private String username;
	
	@NotEmpty(message = "Staff Password is required")
	private String password;

	@NotEmpty(message = "Staff Permission is required")
	@StaffPermission
	@Singular
	private Set<String> permissions;

	@NotEmpty(message = "Staff Full Name is required")
	@Pattern(regexp = "[A-Za-z\\s\\.]+", message = "Staff full name can only contain letters")
	private String fullName;

	@NotEmpty(message = "Staff email is required")
//	@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", 
//		message = "Invalid email format")
	@Email
	private String email;
	
	@Pattern(regexp = "^[0-9]*$", message = "Phone number can only contain numbers")
	private String phone;

}
