package com.cart.common.model;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

	@NotNull(message = "Product ID is required")
	@Min(value = 1, message = "Product ID must be larger than 0")
	private int id;

	@NotEmpty(message = "Product name is required")
	@Pattern(regexp = "[A-Za-z0-9\\s]+", message = "Product name can only contain letters and numbers")
	private String productName;

	@NotNull(message = "Available Quantity is required")
	@Min(value = 1, message = "Available quantity must be larger than 0")
	private Integer availableQuantity;

	@NotNull(message = "Price is required")
	@DecimalMin(value = "1.00", message = "Price cannot be lower than 0")
	private BigDecimal price;
	
	@NotEmpty(message = "Product Image Path is required")
	private String imagePath;

}
