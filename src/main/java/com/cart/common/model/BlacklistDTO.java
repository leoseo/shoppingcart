package com.cart.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class BlacklistDTO {
	
	private String username;
	private String jwt;

}
