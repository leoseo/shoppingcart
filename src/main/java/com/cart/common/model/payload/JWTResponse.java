package com.cart.common.model.payload;

import java.util.List;

import com.cart.common.constant.Constant;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JWTResponse {
	private String token;
	@Builder.Default
	private String tokenType = Constant.TOKEN_TYPE;
	private Integer id;
	private String username;
	private String email;
	private List<String> roles;

}
